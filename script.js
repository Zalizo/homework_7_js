/*
1. Метод forEach дозволяє виконувати певну операцію над кожним елементом масиву
 без використання циклів.
2. Є кілька способів очистити масив:
      1) array.lenght = 0
      2) array.splice()
      3) array.filter()
      4) присвоїти порожній масив
3.  Є кілька способів перевірки:
    1) array.isArray()
    2) array.prototype
    3) array.constructor === Array
*/
function filterBy(arr, dataType) {
  let filteredArr = [];

  for (let i = 0; i < arr.length; i++) {
    if (typeof arr[i] !== dataType) {
      filteredArr.push(arr[i]);
    }
  }

  return filteredArr;
}

let arr = ["hello", "world", 23, "23", null];
let filteredArr = filterBy(arr, "string");

console.log(filteredArr);
